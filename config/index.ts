export const APP_NAME: any = process.env.APP_NAME;
export const API_URL: any = process.env.API_URL;
export const TOKEN_NAME: any = process.env.TOKEN_NAME;
export const USER_KEY: any = process.env.USER_KEY;
export const HTTP_TIMEOUT: any = process.env.HTTP_TIMEOUT;
export const SECRET_KEY: any = process.env.SECRET_KEY;
