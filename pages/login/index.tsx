import { LockOutlined, UserOutlined } from "@ant-design/icons";
import { Player } from "@lottiefiles/react-lottie-player";
import { Button, Card, Form, Input, Typography } from "antd";
import { NextPage } from "next";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import {
  API_URL,
  APP_NAME,
  HTTP_TIMEOUT,
  SECRET_KEY,
  TOKEN_NAME,
  USER_KEY,
} from "../../config";
import { useUnauthenticatedPage } from "../../hooks/authentication";
import { setToken, setUser } from "../../utils/session";

const Login: NextPage = () => {
  useUnauthenticatedPage();

  const [isLoading, setIsLoading] = useState(false);
  const [loginForm] = Form.useForm();
  const router = useRouter();

  const handleLogin = async () => {
    setIsLoading(true);

    await loginForm
      .validateFields()
      .then((values) => {
        setToken(SECRET_KEY);
        setUser(JSON.stringify(values));

        router.replace("/dashboard");
      })
      .catch((e) => {
        console.log(e, "Form Not Valid");
      });

    setIsLoading(false);
  };

  useEffect(() => {
    console.log(
      APP_NAME,
      API_URL,
      TOKEN_NAME,
      USER_KEY,
      HTTP_TIMEOUT,
      SECRET_KEY
    );
  }, []);

  return (
    <div className="h-screen flex flex-row justify-center items-center">
      <Card className="rounded-md">
        <div className="flex flex-col items-center">
          <div className="bg-gray-400 w-20 h-20"></div>
          <Typography.Text className="my-6 text-2xl font-semibold">
            Login Form
          </Typography.Text>

          <Form form={loginForm} name="loginForm" layout="vertical">
            <Form.Item
              label="Email or Username"
              name="username"
              rules={[
                {
                  required: true,
                  message: "Please input Your Username or Email",
                },
              ]}
            >
              <Input
                prefix={<UserOutlined />}
                type="text"
                placeholder="Enter Your Email or Username"
              />
            </Form.Item>
            <Form.Item
              label="Password"
              name="password"
              rules={[
                { required: true, message: "Please input Your Password" },
              ]}
            >
              <Input
                prefix={<LockOutlined />}
                type="text"
                placeholder="Enter Your Password"
              />
            </Form.Item>
            <Button
              type="primary"
              loading={isLoading}
              block
              onClick={handleLogin}
            >
              Login
            </Button>
          </Form>
        </div>
      </Card>
      <div className="w-1/3 ml-16">
        <Player autoplay loop src="/assets/lotties/login.json" />
      </div>
    </div>
  );
};

export default Login;
