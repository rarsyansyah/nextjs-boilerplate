import React from "react";
import "../styles/global.scss";
import type { AppProps } from "next/app";
import { Provider } from "react-redux";
import { applyMiddleware, compose, createStore } from "@reduxjs/toolkit";
import thunk from "redux-thunk";
import reducers from "../store/reducers";
import Head from "next/head";
import { APP_NAME } from "../config";
// import ParticlesBg from "particles-bg";

function MyApp({ Component, pageProps }: AppProps) {
  const store = createStore(reducers, compose(applyMiddleware(thunk)));

  return (
    <Provider store={store}>
      <Head>
        <title>{APP_NAME}</title>
      </Head>
      <Component {...pageProps} />
      {/* <ParticlesBg type="square" bg /> */}
    </Provider>
  );
}

export default MyApp;
