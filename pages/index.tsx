import { NextPage } from "next";
import { useRouter } from "next/router";

const Home: NextPage = () => {
  const router = useRouter();

  if (typeof window !== "undefined") router.push("/login");

  return <div></div>;
};

export default Home;
