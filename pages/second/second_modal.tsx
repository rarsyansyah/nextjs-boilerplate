import React from "react";
import { Modal } from "antd";

const SecondModal = ({ isVisible, handleOK, handleCancel }) => (
  <Modal
    visible={isVisible}
    title="Second Modal"
    onOk={handleOK}
    onCancel={handleCancel}
  >
    Hello World
  </Modal>
);

export default SecondModal;
