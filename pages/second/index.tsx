import { EditOutlined } from "@ant-design/icons";
import { Alert, Breadcrumb, Button, Card, PageHeader, Table } from "antd";
import { NextPage } from "next";
import Link from "next/link";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import MainLayout from "../../components/Layout/main_layout";
import { useAuthenticatedPage } from "../../hooks/authentication";
import { getUsers } from "../../store/actions/user_actions";
import SecondModal from "./second_modal";

const Second: NextPage = () => {
  useAuthenticatedPage();

  const dispatch = useDispatch();
  const { getUsersResult, getUsersLoading, getUsersError } = useSelector(
    (state) => state.user
  );

  const [isVisible, setIsVisible] = useState(false);

  const columns = [
    {
      title: "No",
      key: "no",
      render: (text: string, record: any, idx: number) => <>{idx + 1}</>,
    },
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Age",
      dataIndex: "age",
      key: "age",
    },
    {
      title: "Action",
      key: "action",
      render: () => (
        <Button type="dashed" onClick={handleOpenModal}>
          <EditOutlined /> Edit
        </Button>
      ),
    },
  ];

  const handleOpenModal = () => setIsVisible(true);
  const handleCancel = () => setIsVisible(false);

  useEffect(() => {
    dispatch(getUsers());
  }, [dispatch]);

  return (
    <MainLayout>
      <PageHeader title="Menu 1" />

      <Breadcrumb className="mb-4" separator=">">
        <Breadcrumb.Item href="/dashboard">Dashboard</Breadcrumb.Item>
        <Breadcrumb.Item>Menu 1</Breadcrumb.Item>
      </Breadcrumb>

      <Card className="rounded-lg">
        {getUsersLoading && <Alert type="info">Loading</Alert>}
        {getUsersError && (
          <Alert type="error">{JSON.stringify(getUsersError)}</Alert>
        )}
        {getUsersResult && (
          <Table
            loading={getUsersLoading}
            columns={columns}
            dataSource={getUsersResult}
            pagination={{ total: getUsersResult.length }}
          />
        )}
      </Card>

      <SecondModal
        key="second_modal"
        isVisible={isVisible}
        handleOK={handleCancel}
        handleCancel={handleCancel}
      />
    </MainLayout>
  );
};

export default Second;
