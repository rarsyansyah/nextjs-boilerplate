import { Breadcrumb, PageHeader } from "antd";
import { NextPage } from "next";
import Link from "next/link";
import MainLayout from "../../components/Layout/main_layout";
import { useAuthenticatedPage } from "../../hooks/authentication";
import { getToken, getUser } from "../../utils/session";

const Dashboard: NextPage = () => {
  useAuthenticatedPage();

  return (
    <MainLayout>
      <PageHeader title="Dashboard" />

      <Breadcrumb className="mb-4" separator=">">
        <Breadcrumb.Item>Dashboard</Breadcrumb.Item>
      </Breadcrumb>

      <div className="bg-white px-4 py-2 h-64">
        <p>Content</p>
        <p>{getToken()}</p>
        <p>{getUser()}</p>
      </div>
    </MainLayout>
  );
};

export default Dashboard;
