import { getToken } from "../utils/session";
import { useRouter } from "next/router";

export const useAuthenticatedPage = () => {
  const router = useRouter();

  if (typeof window === "undefined") return;

  const token = getToken();

  if (!token) router.push("/login");
};

export const useUnauthenticatedPage = () => {
  const router = useRouter();

  if (typeof window === "undefined") return;

  const token = getToken();

  if (token) router.push("/dashboard");
};
