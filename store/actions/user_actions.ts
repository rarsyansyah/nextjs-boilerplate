import { GET_USERS } from "./types";
import http from "../../utils/http";

export const getUsers = () => {
  return (dispatch: any) => {
    dispatch({
      type: GET_USERS,
      payload: {
        loading: true,
        data: null,
        error: null,
      },
    });

    http
      .get("/users")
      .then((response) => {
        console.log(response.data, "aowkoaw");

        dispatch({
          type: GET_USERS,
          payload: {
            loading: false,
            data: response.data,
            error: null,
          },
        });
      })
      .catch((error) =>
        dispatch({
          type: GET_USERS,
          payload: {
            loading: false,
            data: null,
            error,
          },
        })
      );
  };
};
