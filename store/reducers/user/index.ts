import { GET_USERS } from "../../actions/types";

const initialState = {
  getUsersResult: null,
  getUsersLoading: false,
  getUssersError: null,
};

const UserReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case GET_USERS:
      return {
        ...state,
        getUsersResult: action.payload.data,
        getUsersLoading: action.payload.loading,
        getUsersError: action.payload.error,
      };

    default:
      return state;
  }
};

export default UserReducer;
