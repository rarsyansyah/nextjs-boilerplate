import axios from "axios";
import { API_URL, HTTP_TIMEOUT } from "../config";
import { getToken } from "./session";

const instance = axios.create({
  baseURL: API_URL,
  timeout: HTTP_TIMEOUT,
  validateStatus: (status) => status >= 200 && status < 400,
});

const HttpHeaders: object = () => {
  const token = getToken();
  const headers = {
    "Content-Type": "application/json",
    Authorization: token ? "Bearer " + token : null,
  };

  return headers;
};

export default {
  get: (url: string) => instance.get(url, HttpHeaders),
  post: (url: string, data = {}) => instance.post(url, data, HttpHeaders),
  put: (url: string, data = {}) => instance.put(url, data, HttpHeaders),
  patch: (url: string, data = {}) => instance.patch(url, data, HttpHeaders),
  delete: (url: string) => instance.delete(url, HttpHeaders),
};
