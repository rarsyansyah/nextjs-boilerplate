import { USER_KEY } from "./../config/index";
import { TOKEN_NAME } from "../config";
import { decrypt, encrypt } from "./crypto";

export const setToken = (token: string) =>
  localStorage.setItem(TOKEN_NAME, token?.toString());

export const getToken = () => {
  if (typeof window === "undefined") return;

  const token = localStorage.getItem(TOKEN_NAME);
  return token ? token?.toString() : null;
};

export const setUser = (user: string) =>
  localStorage.setItem(USER_KEY, user.toString());

export const getUser = () => {
  if (typeof window === "undefined") return;

  const user = localStorage.getItem(USER_KEY);
  return user ? user?.toString() : null;
};

export const clearSession = () => {
  localStorage.removeItem(TOKEN_NAME);
  localStorage.removeItem(USER_KEY);
};
