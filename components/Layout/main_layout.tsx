import { Layout } from "antd";
import MenuLayout from "./menu_layout";
import HeaderLayout from "./header_layout";

const MainLayout = ({ children }) => {
  const { Sider, Content, Footer } = Layout;

  return (
    <Layout>
      <Sider theme="light">
        <div className="flex justify-center my-4">
          <div className="bg-gray-400 w-12 h-12" />
        </div>
        <MenuLayout />
      </Sider>
      <Layout>
        <HeaderLayout />
        <Content className="mx-3">{children}</Content>
        <Footer className="text-center">
          &copy; Rizky Arsyansyah Rinjani - 18.11.0153
        </Footer>
      </Layout>
    </Layout>
  );
};

export default MainLayout;
