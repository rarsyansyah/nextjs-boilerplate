import { QuestionCircleOutlined, UserOutlined } from "@ant-design/icons";
import { Layout, Menu, Modal, Popover, Typography } from "antd";
import Avatar from "antd/lib/avatar/avatar";
import { useRouter } from "next/router";
import { clearSession } from "../../utils/session";

const HeaderLayout = () => {
  const { Header } = Layout;
  const { Text, Paragraph } = Typography;
  const router = useRouter();

  const handleLogoutModal = () =>
    Modal.confirm({
      title: "Are you sure to logout ?",
      icon: <QuestionCircleOutlined />,
      content: "Bla bla bla..",
      onOk: handleLogout,
    });

  const handleLogout = () => {
    clearSession();
    router.replace("/login");
  };

  const popoverTitle = (
    <Text>
      <span className="font-semibold">Your Name</span>
      <Paragraph type="secondary">Admin</Paragraph>
    </Text>
  );

  const popoverContent = (
    <Menu selectable={false}>
      <Menu.Item key="0">Profile</Menu.Item>
      <Menu.Item key="1" onClick={handleLogoutModal}>
        Logout
      </Menu.Item>
    </Menu>
  );

  return (
    <Header className="bg-blue-400 flex justify-end items-center">
      <Popover
        autoAdjustOverflow
        placement="bottomRight"
        title={popoverTitle}
        content={popoverContent}
        trigger="click"
      >
        <Avatar
          size="large"
          className="flex justify-center items-center"
          icon={<UserOutlined />}
        />
      </Popover>
    </Header>
  );
};

export default HeaderLayout;
