import { DashboardOutlined, MenuOutlined } from "@ant-design/icons";
import { Menu } from "antd";
import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";

const MenuLayout = () => {
  const router = useRouter();

  const [selectedMenu, setSelecedMenu] = useState("0");

  const menus = [
    { label: "Dashboard", url: "/dashboard", icon: <DashboardOutlined /> },
    { label: "Second", url: "/second", icon: <MenuOutlined /> },
  ];

  useEffect(() => {
    const route = router.route;

    menus.map((menu, idx) => {
      if (menu.url == route) setSelecedMenu(idx.toString());
    });

    console.log(router.route);
  }, []);

  return (
    <Menu
      mode="inline"
      defaultSelectedKeys={["0"]}
      selectedKeys={[selectedMenu]}
    >
      {menus.map((menu, idx) => (
        <Menu.Item
          key={idx.toString()}
          icon={menu.icon}
          onClick={() => setSelecedMenu(idx.toString())}
        >
          <Link href={menu.url}>{menu.label}</Link>
        </Menu.Item>
      ))}
    </Menu>
  );
};

export default MenuLayout;
