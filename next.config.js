const withAntdLess = require('next-plugin-antd-less');
const antdVariables = require('./styles/antd_variables')

const nextConfig = {
  modifyVars: antdVariables,
  // lessVarsFilePath: './styles/variables.less',
  lessVarsFilePathAppendToEndOfContent: false,
  cssLoaderOptions: {},
  webpack(config) {
    return config;
  },
  reactStrictMode: true,
  env: {
    APP_NAME: process.env.APP_NAME,
    API_URL: process.env.API_URL,
    TOKEN_NAME: process.env.TOKEN_NAME,
    USER_KEY: process.env.USER_KEY,
    HTTP_TIMEOUT: process.env.HTTP_TIMEOUT,
    SECRET_KEY: process.env.SECRET_KEY,
  },
};

module.exports = withAntdLess(nextConfig);
